# Raspberry Pi mouse emulator 

## Setting RPi system

### Flash system

Used system is the Raspberry Pi OS Lite.

#### Prerequisites

* [Raspberry Pi Imager](https://www.raspberrypi.org/software/)
* Micro SD card
* Micro SD card adapter

#### Steps to flash image:

* Plugin micro SD card to the adapter and connect it to PC.
* Start Raspberry Pi Imager program
  * As OS select Raspberry PI Lite 
  * As Target storage select the SD card.
  * Start flashing the image by pushing the button.

### Connect RPi to WiFi for SSH connection

* After flashing the image create file `wpa_supplicant.conf` on `boot` partition with this content:

> Replace `<network_ssid>` and `<network_password>` with proper network name and password
```bash
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=PL

network={
        ssid="<network_ssid>"
        psk="<network_password>"
        scan_ssid=1
}
```
* unmount and unplug micro SD card from PC and insert it do RPi
* Boot RPi - board can be powered directly from PC which will be used as host
  for RPi mouse.
* Wait  couple of seconds (may be even around 1 minute) to RPi connect to WiFi
* Find RPi IP address in your router info.
* Login to RPi using ssh

```shell
$ ssh pi@192.168.1.30
```
> Remember to replace `192.168.1.30` with correct RPi IP address

> Default password for user `pi` is `raspberry`

### Enable USB gadget in the device tree

In `/boot/config.txt` add line:

```bash
dtoverlay=dwc2
```

and reboot.

## Emulating mouse

### Wiring

Set up the wiring as shown on schema:

![wiring_schema](img/mcp3008-wiring.png)

### Create usb device 

Execute provided script `mkdevice` as super-user

```shell
$ sudo ./mkdevice
```

### Start reading joystick data and sending hid reports

```shell
$ sudo ./mouse_emu
```

### Deploying application as service

If you want RPI to start emulating a mouse on the system start you can install app as a service.

* Run `install` script as super-user.

```shell
sudo ./install
```

* Start the service

```shell
sudo systemctl start mouse_emu
```

Manually starting the service is required only one time after performing
installation.
> Alternatively you can reboot your RPI.

### Example setup

![setup](img/setup.jpg)
